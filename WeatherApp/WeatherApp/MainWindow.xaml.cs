﻿using System;
using System.Device.Location;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static WeatherApp.WeatherInfo;
using static WeatherApp.WeatherInfoDaily;
using System.ComponentModel;
using LiveCharts;
using LiveCharts.Wpf;
using System.Reflection;
using WeatherApp.TableClasses;

namespace WeatherApp
{

    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        public string y { get; set; }
        public Func<double, string> yFormatter { get; set; }

        public string Y
        {
            get
            {
                return y;
            }
            set
            {
                if (value != y)
                {
                    y = value;
                    OnPropertyChanged("Y");
                }

            }
        }
        public Func<double, string> YFormatter
        {
            get
            {
                return yFormatter;
            }
            set
            {
                if (value != yFormatter)
                {
                    yFormatter = value;
                    OnPropertyChanged("YFormatter");
                }

            }
        }
        
        public Dictionary<string, RootObjectDaily> locationDailyData = new Dictionary<string, RootObjectDaily>();

        public ObservableCollection<Daily> Week
        {
            get;
            set;
        }
        public ObservableCollection<Tabela> CurrentList
        {
            get;
            set;
        }
        public ObservableCollection<HourlyForecast> Hours
        {
            get;
            set;
        }
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        
        string currentLocation = "";
        bool permissionToDelete = false;

        

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Week = new ObservableCollection<Daily>();
            CurrentList = new ObservableCollection<Tabela>();
            Hours = new ObservableCollection<HourlyForecast>();
            SeriesCollection = new SeriesCollection();
            this.SizeToContent = SizeToContent.WidthAndHeight;
            Labels = new string[10];
            updateCurrent(getCurrentWeather("q=Novi Sad"));
            
            this.Show();
        }

        
        //update vrednosti
        private void updateCurrent(RootObject currentData)
        {
            cityState.Content = currentData.name + "," + currentData.sys.country;
            date.Content = DateTime.Now.ToString("dd. MMM yyyy. HH:mm");
            currentTemp.Content = currentData.main.temp + " °C";
            if (currentData.weather[0].description.Contains(" "))
            {
                string[] words = currentData.weather[0].description.Split(' ');
                this.description.Content = words[0];
                this.description2.Content = words[1];
            }
            else
            {
                description.Content = currentData.weather[0].description;
            }
            min.Content = currentData.main.temp_min + " °C";
            max.Content = currentData.main.temp_max + " °C";
            pressure.Content = currentData.main.pressure+" hPa";
            humidity.Content = currentData.main.humidity+" %";
            visibility.Content = currentData.visibility+" m";
            tempIcon.Source = new BitmapImage(new Uri("http://openweathermap.org/img/wn/" + currentData.weather[0].icon + "@2x.png"));

            Hours.Clear();
            String crds = "lat=" + currentData.coord.lat + "&lon=" + currentData.coord.lon;
            WeatherInfoDaily.RootObjectDaily dd = getDailyWeather(crds);
            currentLocation = currentData.name;
            locationDailyData[currentData.name] = dd;
            int interval = 8;
            try
            {
                interval = Int32.Parse(comboInterval.Text.Substring(0, comboInterval.Text.Length - 6));
            }catch
            {
                interval = 24;
            }
            addHourlyFcst(dd.hourly, interval);
            addCurrent(currentData);
            addWeek(dd.daily, currentData.name);
            addChart();
        }

        //dobavljanje podataka za graf
        private ChartValues<double> getData(int interval, RootObjectDaily daily, String parameter)
        {
            ChartValues<double> result = new ChartValues<double>();
            if (interval == 1)
            {
                interval = 24;
            }
            for (int i = 0; i < interval; i++)
            {
                if (parameter=="Temperature") { 
                    result.Add(daily.hourly[i].temp);
                }
                else if (parameter == "Humidity")
                {
                    result.Add(daily.hourly[i].humidity);
                }
                else
                {
                    result.Add(daily.hourly[i].pressure);
                }
            }
            return result;
        }

        //generisanje stringa koji predstavlja vreme u vremenskom intervalu
        private List<string> generateString(int interval)
        {
            List<string> results = new List<string>();
            int currentHour = DateTime.Now.Hour;
            string value = "";
            for (int i = 0; i < interval; i++)
            {
                if (currentHour == 24) { currentHour = 0; }
                currentHour += 1;
                if (currentHour == 24)
                {
                    currentHour = 0;
                }
                if (currentHour.ToString().Length == 1)
                {
                    value = "0" + currentHour + ":00";
                }
                else
                {
                    value = currentHour + ":00";
                }
                results.Add(value);
            }
            return results;
        }

        //definisanje labela grafa
        private string[] defineLabels(int interval)
        {
            string[] result = new string[interval];
            List<String> time = generateString(interval);
            if (interval==3) {
                result = new[] { time[0], time[1], time[2]};
            }
            else if (interval==8)
            {
                result = new[] { time[0] , time[1], time[2], time[3] , time[4], time[5], time[6] , time[7] };
            }
            else if (interval == 12)
            {
                result = new[] { time[0], time[1], time[2], time[3], time[4], time[5], time[6], time[7], time[8], time[9], time[10], time[11] };
            }
            else {
                result = new[] { time[1], time[3], time[5], time[7], time[9], time[11], time[13], time[15], time[17], time[19], time[21], time[23] };

            }
            return result;
        }

        //dodavanje grafa
        private void addChart()
        {

            if (SeriesCollection != null) { SeriesCollection.Clear(); }
            else { SeriesCollection = new SeriesCollection(); }

            this.Y = comboData.Text;
            if (Y == "Humidity")
            {
                YFormatter = value => value.ToString() + "%";
            }
            if (Y.Equals("Temperature"))
            {
                YFormatter = value => value.ToString() + "°C";
            }
            if (Y.Equals("Pressure"))
            {
                YFormatter = value => value.ToString() + "hPa";
            }

            int interval = 8;
            try {
                interval = Int32.Parse(comboInterval.Text.Substring(0,comboInterval.Text.Length - 6));
            }catch { interval = 24; }

                Labels = defineLabels(interval);

            foreach (Daily d in Week)
            {
                ChartValues<double> resultD = new ChartValues<double>();

                resultD = getData(interval, locationDailyData[d.Location], comboData.Text);

                    SeriesCollection.Add(new LineSeries
                    {
                        Title = d.Location,
                        Values = resultD,
                        PointForeground = PickBrush()
                    });
            }
            if (Week.Count==0 || (Week.Count==1 && !Week[0].Location.Equals(currentLocation)))
            {
                ChartValues<double> resultD = new ChartValues<double>();

                resultD = getData(interval, locationDailyData[currentLocation], comboData.Text);

                SeriesCollection.Add(new LineSeries
                {
                    Title = currentLocation,
                    Values = resultD,
                    PointForeground = PickBrush()
                });
            }

        }

        //random odabir boje kod fje grafa
        private Brush PickBrush()
        {
            Brush result = Brushes.Transparent;

            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);
            
            return result;
        }

        //konvertovanje temperature sa apija, na 0 decimala + "°C" 
        private string convert(string temp)
        {
            if (temp.Length>2)
            {
                temp = temp.Substring(0, temp.Length - 3);
            }

            return temp + "°C";
        }

        //dodavanje u listu za sate
        private void addHourlyFcst(List<WeatherInfoDaily.Hourly> hourlyData, int interval)
        {
            int currentHour = DateTime.Now.Hour;
            List<string> time = generateString(interval);
            for (int i = 0; i < interval; i++)
            {
                
                Hours.Add(new HourlyForecast { Time = time[i], Temp = hourlyData[i].temp + "°C", Icon= string.Format("http://openweathermap.org/img/wn/{0}.png", hourlyData[i].weather[0].icon), Humidity=hourlyData[i].humidity+"%", IconHumidity="../../Resources/wi-raindrop.png", Pressure=hourlyData[i].pressure+" hPa" });
            }
        }
        private void addCurrent(RootObject currentData)
        {
            foreach (Tabela c in CurrentList)
            {
                if (c.Location.Equals(currentData.name))
                    return;
            }
            
            CurrentList.Add(new Tabela
            {
                Img = string.Format("http://openweathermap.org/img/wn/{0}.png", currentData.weather[0].icon),
                Location = currentData.name,
                Temp = convert((currentData.main.temp).ToString()),
                Min = convert((currentData.main.temp_min).ToString()),
                Max = convert((currentData.main.temp_max).ToString()),
                Humidity = currentData.main.humidity + "%",
                Pressure = currentData.main.pressure + "hPa",
                Visibility = currentData.visibility + "m",
                Delete = "../../Resources/delete.png"

            });
        }

        //dodavanje u listu sedmicnog vremena
        private void addWeek(List<WeatherInfoDaily.Daily> dailyData, string city)
        {
            foreach (Daily d in Week)
            {
                if (d.Location.Equals(city))
                    return;
            }
            Week.Add(new Daily { ImgMon = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[0].weather[0].icon),
                ImgThu = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[1].weather[0].icon),
                ImgWed = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[2].weather[0].icon),
                ImgTue = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[3].weather[0].icon),
                ImgFri = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[4].weather[0].icon),
                ImgSat = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[5].weather[0].icon),
                ImgSun = string.Format("http://openweathermap.org/img/wn/{0}.png", dailyData[6].weather[0].icon),
                Delete = "../../Resources/delete.png",
                Location = city, Mon = dailyData[0].temp.day + "°C", Thu = dailyData[1].temp.day + "°C", Wed = dailyData[2].temp.day + "°C", Tue = dailyData[3].temp.day + "°C", Fri = dailyData[4].temp.day + "°C", Sat = dailyData[5].temp.day + "°C", Sun = dailyData[6].temp.day + "°C"});

        }

        
        //dobavljanje sedmicnog vremena
        RootObjectDaily getDailyWeather(String coords)
        {
            using (WebClient web = new WebClient())
            {
                string url7Days = string.Format("https://api.openweathermap.org/data/2.5/onecall?" + coords+ "&appid=35aee5ca4974c44f019cbd18db0f7748&units=metric");
                var json7Days = web.DownloadString(url7Days);
                WeatherInfoDaily.RootObjectDaily dailyResult = JsonConvert.DeserializeObject<WeatherInfoDaily.RootObjectDaily>(json7Days);
                return dailyResult;
            }

        }

        //dobavljanje trenutnog vremena
        WeatherInfo.RootObject getCurrentWeather(String parameter)
        {
            using (WebClient web = new WebClient())
            {  
                string urlCurrent = string.Format("https://api.openweathermap.org/data/2.5/weather?" + parameter + "&appid=35aee5ca4974c44f019cbd18db0f7748&units=metric");
                var jsonCurrent = web.DownloadString(urlCurrent);
                WeatherInfo.RootObject currentResult = JsonConvert.DeserializeObject<WeatherInfo.RootObject>(jsonCurrent);
                return currentResult;
            }
        }

        //funkcija za search button
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string cityLower = city.Text.ToLower();
                string cityUpper = cityLower.First().ToString().ToUpper() + cityLower.Substring(1);
                WeatherInfo.RootObject currentData = getCurrentWeather("q="+cityUpper);
                String coords = "lat=" + currentData.coord.lat + "&lon=" + currentData.coord.lon;
                WeatherInfoDaily.RootObjectDaily dailyData = getDailyWeather(coords);
                updateCurrent(currentData);

            } catch
            {
                MessageBox.Show("Invalid location.");
            }
        }

        //funkcija za brisanje iz tabele
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            
            Tabela t = weekTime.SelectedItem as Tabela;
            permissionToDelete = true;
            if (CurrentList.Count==1)
            {
                return;
            }
            CurrentList.Remove(t);

                Daily dailyToRemove = null;

                foreach (Daily d in Week)
                {
                    if (d.Location.Equals(t.Location))
                    {
                        dailyToRemove = d;
                    }
                }
                if (dailyToRemove != null) { Week.Remove(dailyToRemove); }
                if (dailyToRemove != null && dailyToRemove.Location.Equals(currentLocation))
                {
                    updateCurrent(getCurrentWeather("q=" + CurrentList[0].Location));
                }
                else
                {
                updateCurrent(getCurrentWeather("q=" + currentLocation));
                }
                permissionToDelete = false;
                
        }

        //refresh grafa
        private void ApplyGraph_Click(object sender, RoutedEventArgs e)
        {
            updateCurrent(getCurrentWeather("q="+currentLocation));
        }

        //promena selekcije u tabeli, apdejt podataka
        private void WeekTime_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (permissionToDelete == true)
            {
                return;
            }
            else
            {
                DataGrid dg = (DataGrid)sender;
                Tabela t = dg.SelectedItem as Tabela;
                if (t != null)
                {
                    updateCurrent(getCurrentWeather("q=" + t.location));
                }
            }
        }
    }
}
