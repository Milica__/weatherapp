﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp
{
    public class Daily : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        private string delete;

        private string img_mon;
        private string img_thu;
        private string img_wed;
        private string img_tue;
        private string img_fri;
        private string img_sat;
        private string img_sun;


        private string _location;
        private string _mon;
        private string _thu;
        private string _wed;
        private string _tue;
        private string _fri;
        private string _sat;
        private string _sun;


        public string Delete
        {
            get
            {
                return delete;
            }
            set
            {
                if (value != delete)
                {
                    delete = value;
                    OnPropertyChanged("Delete");
                }

            }
        }

        public string ImgMon
        {
            get
            {
                return img_mon;
            }
            set
            {
                if (value != img_mon)
                {
                    img_mon = value;
                    OnPropertyChanged("ImgMon");
                }

            }
        }

        public string ImgThu
        {
            get
            {
                return img_thu;
            }
            set
            {
                if (value != img_thu)
                {
                    img_thu = value;
                    OnPropertyChanged("ImgThu");
                }

            }
        }

        public string ImgWed
        {
            get
            {
                return img_wed;
            }
            set
            {
                if (value != img_wed)
                {
                    img_wed = value;
                    OnPropertyChanged("ImgWed");
                }
            }
        }

        public string ImgTue
        {
            get
            {
                return img_tue;
            }
            set
            {
                if (value != img_tue)
                {
                    img_tue = value;
                    OnPropertyChanged("ImgTue");
                }

            }
        }

        public string ImgFri
        {
            get
            {
                return img_fri;
            }
            set
            {
                if (value != img_fri)
                {
                    img_fri = value;
                    OnPropertyChanged("ImgFri");
                }

            }
        }

        public string ImgSat
        {
            get
            {
                return img_sat;
            }
            set
            {
                if (value != img_sat)
                {
                    img_sat = value;
                    OnPropertyChanged("ImgSat");
                }

            }
        }

        public string ImgSun
        {
            get
            {
                return img_sun;
            }
            set
            {
                if (value != img_sun)
                {
                    img_sun = value;
                    OnPropertyChanged("ImgSun");
                }

            }
        }


        public string Location
        {
            get
            {
                return _location;
            }
            set
            {
                if (value != _location)
                {
                    _location= value;
                    OnPropertyChanged("Location");
                }

            }
        }
        public string Mon
        {
            get
            {
                return _mon;
            }
            set
            {
                if (value != _mon)
                {
                    _mon = value;
                    OnPropertyChanged("Mon");
                }
            }
        }
        public string Thu
        {
            get
            {
                return _thu;
            }
            set
            {
                if (value != _thu)
                {
                    _thu = value;
                    OnPropertyChanged("Thu");
                }
            }
        }

        public string Wed
        {
            get
            {
                return _wed;
            }
            set
            {
                if (value != _wed)
                {
                    _wed = value;
                    OnPropertyChanged("Wed");
                }
            }
        }

        public string Tue
        {
            get
            {
                return _tue;
            }
            set
            {
                if (value != _tue)
                {
                    _tue = value;
                    OnPropertyChanged("Tue");
                }
            }
        }

        public string Fri
        {
            get
            {
                return _fri;
            }
            set
            {
                if (value != _fri)
                {
                    _fri = value;
                    OnPropertyChanged("Fri");
                }
            }
        }

        public string Sat
        {
            get
            {
                return _sat;
            }
            set
            {
                if (value != _sat)
                {
                    _sat = value;
                    OnPropertyChanged("Sat");
                }
            }
        }

        public string Sun
        {
            get
            {
                return _sun;
            }
            set
            {
                if (value != _sun)
                {
                    _sun = value;
                    OnPropertyChanged("Sun");
                }
            }
        }
    }
}
