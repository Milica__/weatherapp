﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.TableClasses
{
    public class Tabela : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public string location;
        public string temp;
        public string maxTemp;
        public string minTemp;
        public string pressure;
        public string humidity;
        public string visibility;
        private string img;
        private string delete;

        public string Delete
        {
            get
            {
                return delete;
            }
            set
            {
                if (value != delete)
                {
                    delete = value;
                    OnPropertyChanged("Delete");
                }

            }
        }

        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                if (value != location)
                {
                    location = value;
                    OnPropertyChanged("Location");
                }

            }
        }

        public string Img
        {
            get
            {
                return img;
            }
            set
            {
                if (value != img)
                {
                    img = value;
                    OnPropertyChanged("Img");
                }

            }
        }

        public string Temp
        {
            get
            {
                return temp;
            }
            set
            {
                if (value != temp)
                {
                    temp = value;
                    OnPropertyChanged("Temp");
                }

            }
        }

        public string Min
        {
            get
            {
                return minTemp;
            }
            set
            {
                if (value != minTemp)
                {
                    minTemp = value;
                    OnPropertyChanged("Min");
                }

            }
        }

        public string Max
        {
            get
            {
                return maxTemp;
            }
            set
            {
                if (value != maxTemp)
                {
                    maxTemp = value;
                    OnPropertyChanged("Max");
                }

            }
        }

        public string Pressure
        {
            get
            {
                return pressure;
            }
            set
            {
                if (value != pressure)
                {
                    pressure = value;
                    OnPropertyChanged("Pressure");
                }

            }
        }

        public string Humidity
        {
            get
            {
                return humidity;
            }
            set
            {
                if (value != humidity)
                {
                    humidity = value;
                    OnPropertyChanged("Humidity");
                }

            }
        }

        public string Visibility
        {
            get
            {
                return visibility;
            }
            set
            {
                if (value != visibility)
                {
                    visibility = value;
                    OnPropertyChanged("Visibility");
                }

            }
        }






    }
}
