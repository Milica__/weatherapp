﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp
{
    public class HourlyForecast : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        private string _time;
        private string _temp;
        private string icon;
        private string _iconHumidity;
        private string _humidity;
        private string _pressure;

        public string Humidity
        {
            get
            {
                return _humidity;
            }
            set
            {
                if (value != _humidity)
                {
                    _humidity = value;
                    OnPropertyChanged("Humidity");
                }

            }
        }

        public string Pressure
        {
            get
            {
                return _pressure;
            }
            set
            {
                if (value != _pressure)
                {
                    _pressure = value;
                    OnPropertyChanged("Pressure");
                }

            }
        }
        public string IconHumidity
        {
            get
            {
                return _iconHumidity;
            }
            set
            {
                if (value != _iconHumidity)
                {
                    _iconHumidity = value;
                    OnPropertyChanged("IconHumidity");
                }

            }
        }
        public string Icon
        {
            get
            {
                return icon;
            }
            set
            {
                if (value != icon)
                {
                    icon = value;
                    OnPropertyChanged("Icon");
                }

            }
        }

        public string Time
        {
            get
            {
                return _time;
            }
            set
            {
                if (value != _time)
                {
                    _time = value;
                    OnPropertyChanged("Time");
                }

            }
        }
        public string Temp
        {
            get
            {
                return _temp;
            }
            set
            {
                if (value != _temp)
                {
                    _temp = value;
                    OnPropertyChanged("Temp");
                }
            }
        }

    }
}
